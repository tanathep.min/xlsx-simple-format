# xlsx-simple-format

Use for read/write xlsx that have simple format. Will make you handle excel easier and faster
![image info](simple-format-excel.png)
## Todo
- save file to path(anywhere)
- read file from path(anywhere)
- header style
- cell style
- column style

## Guide
[Installation](#installation)
<br />
[Registry setup](#registry-setup)
<br />
[Usage](#usage)
- [Basic Usage(must know)](#basic)
- [ExcelHandler](#excelhandler)
    - [Excel(class)](#excel))
    - [example read excel to object](#example-read-excel-to-object)
        - [ReadNormalFormatExcelSheet](#readnormalformatexcelsheet)
            - [for read with sheet name](#for-read-with-sheet-name)
            - [for read with sheet index](#for-read-with-sheet-index)
            - [for read from InputStream](#for-read-from-inputstream)
    - [example create excel from object](#example-create-excel-from-object)
        - [createNormalFormatAndSaveExcelFile](#createnormalformatandsaveexcelfile)

## Installation
Copy and paste this inside your pom.xml dependencies block.(use only poi version 5.2.0 for avoid confilct)
```
<dependency>
  <groupId>org.tanathap</groupId>
  <artifactId>xlsx-simple-format</artifactId>
  <version>1.1</version>
</dependency>
<dependency>
    <groupId>org.apache.poi</groupId>
    <artifactId>poi</artifactId>
    <version>5.2.0</version>
</dependency>
```
## Maven Command
```
mvn install
```
## Registry setup

If you haven't already done so, you will need to add the below to your pom.xml file.
```
<repositories>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/projects/52299744/packages/maven</url>
  </repository>
</repositories>

<distributionManagement>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/projects/52299744/packages/maven</url>
  </repository>

  <snapshotRepository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/projects/52299744/packages/maven</url>
  </snapshotRepository>
</distributionManagement>
```
## Usage
### Basic
you should create your own sheet format class that implements IExcel and Override `mapperRowPerRecord()` for Read Excel file,  `getOrderFormat()` for Create New Excel file from this class (must be arrange in the correct order) and `getDefaultHeaders()` for verify header(must be arrange in the correct order). and create class attribute that you want to store
{- careful about return type -}
```
public class ExampleData implements IExcel{
    private Integer id;
    private String name;
    private LocalDate start_date;

    @Override
    public List<String> getDefaultHeaders() {
        return Arrays.asList("id", "name", "start_date");
    }

    @Override
    public <T> List<T> getOrderFormat() {
        List record = new ArrayList<>();
        record.add(id);
        record.add(name);
        record.add(start_date);
        return record;
    }

    @Override
    public ExampleData mapperRowPerRecord(Row row) {
        ExampleData record = new ExampleData();

        LocalDate date;
        if (getCellValue(row, 2) instanceof String){
            final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d/M/yyyy");
            date = LocalDate.parse(getCellValue(row, 2).toString(), dtf);
        } else {
            date = ((LocalDateTime) getCellValue(row, 2)).toLocalDate();
        }

        record.setId((int) Math.round((double) getCellValue(row, 0)));
        record.setName(getCellValue(row, 1).toString());
        record.setStart_date(date);

        return record;
    }
}
```
---
### ExcelHandler

#### example read excel to object
```
ExcelSheet readExcel = ExcelHandler.ReadNormalFormatExcelSheet("example-data.xlsx", 0, true, new ExampleData());
List<ExampleData> convertData = readExcel.getData();
readExcel.getSheetName();
```
#### ReadNormalFormatExcelSheet
arg explain
- filename = filepath from this project
- sheetName = sheet name as String
- sheetIdx  = sheet index as int
- hasHeaders = if this sheet has header or not(if has will skip first row)
- mapper = object create from class that implements from IExcel can see how to create from [Basic](#basic) (you can just create new object for use as template for this function like in example)
<br />

#### for read with sheet name
```
ReadNormalFormatExcelSheet(String filename, String sheetName, Boolean hasHeaders, IExcel mapper)
```
#### for read with sheet index
```
ReadNormalFormatExcelSheet(String filename, Integer sheetIdx, Boolean hasHeaders, IExcel mapper)
```
#### for read from InputStream
```
ReadNormalFormatExcelSheet(InputStream fileReader, Integer sheetIdx, Boolean hasHeaders, IExcel mapper)
```
---
### createNormalFormatAndSaveExcelFile
#### example create excel from object
```
List<ExampleData> createData = new ArrayList<>();
createData.add(new ExampleData().setId(111).setName("test111").setStart_date(LocalDate.now()));
createData.add(new ExampleData().setId(222).setName("test222").setStart_date(LocalDate.now()));
ExcelSheet sheet = new ExcelSheet("Sheet1").setHeaders(createData.get(0).getDefaultHeaders());
sheet.setData(createData);
Excel data = new Excel("test_create").setSheets(Collections.singletonList(sheet));
ExcelHandler.createNormalFormatAndSaveExcelFile(data, false);
```
#### Excel
have to pass sheet name like example `will be the name of file when save`
<br />

#### createNormalFormatAndSaveExcelFile
arg explain
- data = object of Excel class that have data that you want like in [Example create excel from object](#example_create_excel_from_object)
- isTemp = Boolean if true will not save file

`Now file will save at root of project only!!`
<br />

```
ByteArrayOutputStream createNormalFormatAndSaveExcelFile(Excel data, Boolean isTemp)
```


----
Hope this will be helpful. Have Fun!. Found any bug or suggestion contact tanathep.min@gmail.com.
