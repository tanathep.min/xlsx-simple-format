package org.tanathap.utils;

import org.tanathap.model.Excel;
import org.tanathap.model.ExcelSheet;
import org.tanathap.model.IExcel;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.IntStream;

public class ExcelHandler {
    public static ByteArrayOutputStream createNormalFormatAndSaveExcelFile(Excel data, Boolean isTemp) throws IOException {
        Workbook workbook = new XSSFWorkbook();
        CreationHelper createHelper = workbook.getCreationHelper();

        for (ExcelSheet sheetData : data.getSheets()) {

            Sheet sheet = workbook.createSheet(sheetData.getSheetName());
            sheet.setColumnWidth(0, 6000);
            sheet.setColumnWidth(1, 4000);

            Row headers = sheet.createRow(0);
            if (sheetData.getHeaders() != null) {
                IntStream.range(0, sheetData.getHeaders().size()).forEach(idx -> {
                            Cell headerCell = headers.createCell(idx);
                            headerCell.setCellValue(sheetData.getHeaders().get(idx));
                        }
                );
            }

            CellStyle dateCellStyle = workbook.createCellStyle();
            CellStyle dateTimeCellStyle = workbook.createCellStyle();
            short dateFormat = createHelper.createDataFormat().getFormat("dd/MM/yyyy");
            short dateTimeFormat = createHelper.createDataFormat().getFormat("dd/MM/yyyy HH:mm:ss");
            dateCellStyle.setDataFormat(dateFormat);
            dateTimeCellStyle.setDataFormat(dateTimeFormat);

            for (int i = 0; i < sheetData.getData().size(); i++) {
                List newRecordData = sheetData.getDataOrderListByIndex(i);
                Row row = sheet.createRow(i + 1);
                IntStream.range(0, newRecordData.size()).forEach(idx -> {
                    Cell cell = row.createCell(idx);
                    if (newRecordData.get(idx) instanceof Date) {
                        cell.setCellValue((Date) newRecordData.get(idx));
                        cell.setCellStyle(dateTimeCellStyle);
                    } else if (newRecordData.get(idx) instanceof String) {
                        cell.setCellValue((String) newRecordData.get(idx));
                    } else if (newRecordData.get(idx) instanceof LocalDateTime) {
                        cell.setCellValue((LocalDateTime) newRecordData.get(idx));
                        cell.setCellStyle(dateTimeCellStyle);
                    } else if (newRecordData.get(idx) instanceof LocalDate) {
                        cell.setCellValue((LocalDate) newRecordData.get(idx));
                        cell.setCellStyle(dateCellStyle);
                    } else if (newRecordData.get(idx) instanceof Calendar) {
                        cell.setCellValue((Calendar) newRecordData.get(idx));
                    } else if (newRecordData.get(idx) instanceof RichTextString) {
                        cell.setCellValue((RichTextString) newRecordData.get(idx));
                    } else if (newRecordData.get(idx) instanceof Boolean) {
                        cell.setCellValue((Boolean) newRecordData.get(idx));
                    } else if (newRecordData.get(idx) instanceof Double) {
                        cell.setCellValue((Double) newRecordData.get(idx));
                    } else if (newRecordData.get(idx) instanceof Integer) {
                        cell.setCellValue((Integer) newRecordData.get(idx));
                    } else if (newRecordData.get(idx) instanceof Float) {
                        cell.setCellValue((Float) newRecordData.get(idx));
                    }
                });
            }
        }


//        Sheet sheet = workbook.createSheet("Persons");
//        sheet.setColumnWidth(0, 6000);
//        sheet.setColumnWidth(1, 4000);
//
//        Row header = sheet.createRow(0);

//        CellStyle headerStyle = workbook.createCellStyle();
//        headerStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
//        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
//
//        XSSFFont font = ((XSSFWorkbook) workbook).createFont();
//        font.setFontName("Arial");
//        font.setFontHeightInPoints((short) 16);
//        font.setBold(true);
//        headerStyle.setFont(font);

//        Cell headerCell = header.createCell(0);
//        headerCell.setCellValue("Name");
//        headerCell.setCellStyle(headerStyle);

//        headerCell = header.createCell(1);
//        headerCell.setCellValue("Age");
//        headerCell.setCellStyle(headerStyle);


        File currDir = new File(".");
        String path = currDir.getAbsolutePath();
        String fileLocation = path.substring(0, path.length() - 1) + data.getFilename();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        // fill the OutputStream with the Excel content
        workbook.write(baos);
        if (!isTemp) {
            SaveExcelFile(workbook, fileLocation);
        }
        return baos;
    }

    public static void SaveExcelFile(Workbook workbook, String fileLocation) throws IOException {
        FileOutputStream outputStream = new FileOutputStream(fileLocation);
        workbook.write(outputStream);
        workbook.close();
    }

    public static ExcelSheet ReadNormalFormatExcelSheet(String filename, String sheetName, Boolean hasHeaders, IExcel mapper) throws IOException {
        File currDir = new File("./" + filename);
        String path = currDir.getAbsolutePath();
        Workbook workbook = new XSSFWorkbook(path);
        Sheet sheet = workbook.getSheetAt(workbook.getSheetIndex(sheetName));
        workbook.close();
        return ReadNormalFormatExcelSheetFunc(sheet, hasHeaders, mapper);
    }

    public static ExcelSheet ReadNormalFormatgExcelSheet(String filename, Integer sheetIdx, Boolean hasHeaders, IExcel mapper) throws IOException {
        File currDir = new File("./" + filename);
        String path = currDir.getAbsolutePath();
        Workbook workbook = new XSSFWorkbook(path);
        Sheet sheet = workbook.getSheetAt(sheetIdx);
        workbook.close();
        return ReadNormalFormatExcelSheetFunc(sheet, hasHeaders, mapper);
    }

    private static ExcelSheet ReadNormalFormatExcelSheetFunc(Sheet sheet, Boolean hasHeaders, IExcel mapper) {
        ExcelSheet dataSheet = new ExcelSheet(sheet.getSheetName());
        List<? extends IExcel> allData = new ArrayList<>();

        int i = 0;
        for (Row row : sheet) {
            if (i == 0) {
                if (hasHeaders) {
                    List<String> headers = new ArrayList<>();
                    for (Cell cell : row) {
                        switch (cell.getCellType()) {
                            case STRING:
                                headers.add(cell.getStringCellValue());
                                break;
                            default:
                                headers.add("");
                                break;
                        }
                    }
                    dataSheet.setHeaders(headers);
                }
                i++;
                continue;
            }
            allData.add(mapper.mapperRowPerRecord(row));
            i++;
        }
        dataSheet.setData(allData);
        if (allData.size() != 0) {
            if (hasHeaders && !dataSheet.isValidateHeader(dataSheet.getDefaultHeaders())) {
                throw new RuntimeException("Excel Headers Mismatch");
            }
        }
        return dataSheet;
    }


    public static ExcelSheet ReadNormalFormatExcelSheet(InputStream fileReader, Integer sheetIdx, Boolean hasHeaders, IExcel mapper) throws IOException {

        Workbook workbook = new XSSFWorkbook(fileReader);
        Sheet sheet = workbook.getSheetAt(sheetIdx);
        workbook.close();
        return ReadNormalFormatExcelSheetFunc(sheet, hasHeaders, mapper);
    }
}


