package org.tanathap;

import org.tanathap.model.ExampleData;
import org.tanathap.model.Excel;
import org.tanathap.model.ExcelSheet;
import org.tanathap.utils.ExcelHandler;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        System.out.println("Hello world!!");

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Example of read file
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
//        ExcelSheet readExcel = ExcelHandler.ReadNormalFormatExcelSheet("example-data.xlsx", 0, true, new ExampleData());
//        List<ExampleData> convertData = readExcel.getData();
//        for (ExampleData data :convertData
//             ) {
//            System.out.println(data);
//        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Example of create file
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
//        List<ExampleData> createData = new ArrayList<>();
//        createData.add(new ExampleData().setId(111).setName("test111").setStart_date(LocalDate.now()));
//        createData.add(new ExampleData().setId(222).setName("test222").setStart_date(LocalDate.now()));
//
//        ExcelSheet sheet = new ExcelSheet("Sheet1").setHeaders(createData.get(0).getDefaultHeaders());
//        sheet.setData(createData);
//        Excel data = new Excel("test_create").setSheets(Collections.singletonList(sheet));
//        ExcelHandler.createNormalFormatAndSaveExcelFile(data, false);
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////

    }
}