package org.tanathap.model;

import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.poi.ss.usermodel.Row;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
@Data
@Accessors(chain = true)
public class ExampleData implements IExcel{
    private Integer id;
    private String name;
    private LocalDate start_date;

    @Override
    public List<String> getDefaultHeaders() {
        return Arrays.asList("id", "name", "start_date");
    }

    @Override
    public <T> List<T> getOrderFormat() {
        List record = new ArrayList<>();
        record.add(id);
        record.add(name);
        record.add(start_date);
        return record;
    }

    @Override
    public ExampleData mapperRowPerRecord(Row row) {
        ExampleData record = new ExampleData();

        LocalDate date;
        if (getCellValue(row, 2) instanceof String){
            final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d/M/yyyy");
            date = LocalDate.parse(getCellValue(row, 2).toString(), dtf);
        } else {
            date = ((LocalDateTime) getCellValue(row, 2)).toLocalDate();
        }

        record.setId((int) Math.round((double) getCellValue(row, 0)));
        record.setName(getCellValue(row, 1).toString());
        record.setStart_date(date);

        return record;
    }
}
