package org.tanathap.model;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class ExcelSheet {
    @NonNull
    private String sheetName;
    private List<String> headers;
    private List<String> defaultHeaders;
    private List<? extends IExcel> data;

    public ExcelSheet(@NonNull String sheetName) {
        this.sheetName = sheetName;
    }

    public <T> List<T> getDataOrderListByIndex(Integer index) {
        return data.get(index).getOrderFormat();
    }

    public Boolean isValidateHeader(List<String> input) {
        if (input == null || input.size() != headers.size()) {
            return false;
        }
        for (int i = 0; i < input.size(); i++) {
            if (!input.get(i).equals(headers.get(i))) {
                return false;
            }
        }
        return true;
    }

    public <T> List<T> getData() {
        return (List<T>) data;
    }

    public ExcelSheet setData(List<? extends IExcel> data) {
        this.data = data;
        if (data != null && data.size() != 0) {
            this.defaultHeaders = data.get(0).getDefaultHeaders();
        }
        return this;
    }

    public Boolean hasData() {
        return data != null && data.size() != 0;
    }

    public Boolean isEmptySheet() {
        return (headers != null && headers.size() != 0) || (data != null && data.size() != 0);
    }
}
