package org.tanathap.model;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class Excel {
    @NonNull
    private String filename;
    private List<ExcelSheet> sheets;

    public Excel(String filename) {
        if (filename.endsWith(".xlsx")) {
            this.filename = filename;
        } else {
            this.filename = filename + ".xlsx";
        }
    }
}


